package AccuWeather;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePageSearch {
    WebDriver driver;

    public HomePageSearch(WebDriver driver) {
        this.driver = driver;
    }

    By searchC = By.className("search-input");

    public void search(String search) {
        driver.findElement(searchC).sendKeys(search);
    }
}
