package AccuWeather;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.util.concurrent.TimeUnit;


public class MainUI {

    public String searchCity() throws AWTException {

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-notifications");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        driver.get("https://www.accuweather.com/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //Home Search
        HomePageSearch homePageSearch = new HomePageSearch(driver);
        driver.findElement(homePageSearch.searchC).sendKeys("Kolar, Karnataka" + Keys.RETURN);

        //WeatherPage
        WeatherPage weatherPage = new WeatherPage(driver);
        driver.findElement(weatherPage.temperature).click();
        //ad
        Actions actions = new Actions(driver);
        Robot robot = new Robot();
        robot.mouseMove(885, (int) 3464.310);
        actions.click().build().perform();

        //Result Page
        ResultPage resultPage = new ResultPage(driver);
        String finalTemp = driver.findElement(resultPage.temperatureUI).getText();

        driver.quit();

        return finalTemp;


    }
}

