package AccuWeather;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class ResultPage {
    WebDriver driver;

    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }

    By temperatureUI = By.className("display-temp");
    By humidityUI = By.cssSelector("body > div > div.two-column-page-content > div.page-column-1 > div.content-module > div.current-weather-card.card-module.content-module.non-ad > div.current-weather-details > div.left > div:nth-child(3) > div:nth-child(2)");
    By pressureUI = By.cssSelector("body > div > div.two-column-page-content > div.page-column-1 > div.content-module > div.current-weather-card.card-module.content-module.non-ad > div.current-weather-details > div.right > div:nth-child(1) > div:nth-child(2)");

    public void temp1() {
        driver.findElement(temperatureUI).getText();
    }

    public void setHumidity1() {
        driver.findElement(humidityUI).getText();
    }

    public void setPressure1() {
        driver.findElement(pressureUI).getText();
    }
}
