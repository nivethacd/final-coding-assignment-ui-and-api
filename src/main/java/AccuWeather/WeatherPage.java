package AccuWeather;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WeatherPage {
    WebDriver driver;

    public WeatherPage(WebDriver driver) {
        this.driver = driver;
    }

    By temperature = By.className("temp");
    By closeAd = By.cssSelector("#dismiss-button");

    public void temp() {
        driver.findElement(temperature).click();
    }

    public void setCloseAd(){
        driver.findElement(closeAd).click();
    }

}
