package OpenWeatherMapAPI;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class MainAPI {

    public Response doGetRequest(String endpoint) {
        RestAssured.defaultParser = Parser.JSON;

        return
                given()
                        .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
                        .when()
                        .queryParam("q", "kolar")
                        .queryParam("appid","7fe67bf08c80ded756e598d6f8fedaea")
                        .queryParam("units", "metric")
                        .get(endpoint)
                        .then()
                        .contentType(ContentType.JSON)
                        .extract()
                        .response();
    }
}
