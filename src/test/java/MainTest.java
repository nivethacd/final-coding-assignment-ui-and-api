import AccuWeather.MainUI;
import OpenWeatherMapAPI.MainAPI;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import java.awt.*;
import java.util.Map;

public class MainTest {
    @Test
    public void weatherTest() throws AWTException {
        //AccuWeather
        MainUI mainUI = new MainUI();
        String temp = mainUI.searchCity();
        float tempUI = Float.parseFloat(temp.replaceAll("[^\\d]", ""));

        //OpenWeatherMap
        MainAPI mainAPI = new MainAPI();
        Response response = mainAPI.doGetRequest("https://api.openweathermap.org/data/2.5/weather");
        Map<String, Float> APITemp = response.jsonPath().getMap("main");

        System.out.println("UI(AccuWeather) temperature: " + tempUI);
        System.out.println("API(OpenWeatherMap) temperature: " + APITemp.get("temp"));

        //Comparing and finding difference
        float diff = (tempUI - (APITemp.get("temp")));
        System.out.println("Difference between AccuWeather and OpenWeatherMap temperature is: " + diff);

        if (diff <= 3 && diff >= -3){
            System.out.println("Test Passed");
        }
        else{
            System.out.println("Test Failed");
        }
    }
}
